<?php get_header(); ?>
<section class="steps" id="steps">
    <div class="container d-flex justify-content-center align-items-center" id="loader">
        <img class="center" src="<?= get_template_directory_uri(); ?>/assets/images/loader.svg" alt="Loading...">
    </div>
    <div class="container d-none" id="quiz-box">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="show-section">
                    <?php get_template_part('template-parts/login'); ?>
                    <?php get_template_part('template-parts/questionnaire'); ?>
                    <?php get_template_part('template-parts/score'); ?>
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="show-section">
                    <section class="steps-inner">
                        <div class="wrapper">
                            <div class="step-heading mb-4">
                                <h2>Scoreboard</h2>
                            </div>
                            <div class="table-responsive">
                                <table id="score-board" class="table table-striped table-bordered" style="width:100%">

                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </div>
</section>
<?php get_footer(); ?>