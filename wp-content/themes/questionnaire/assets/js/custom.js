// next prev function
$(document).ready(function () {
  var divs = $('.show-section>.stepss')
  divs.hide().first().show() // hide all divs except first
})


function storeEmail(email) {
  var divs = $('.show-section>.steps-inner')
  var now = 0 // currently shown div
  localStorage.setItem('userEmail', email)
  divs.eq(now).hide()
  now = (now + 1 < divs.length) ? now + 1 : 0
  divs.eq(now).show() // show next
}
function validateEmail(email) {
  var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  if (email.match(mailformat)) {
    return true
  }
  else {
    return false
  }
}
// Store email address
$("#email_form").submit(function (e) {
  e.preventDefault()
  const email = $("#email").val()
  if (validateEmail(email)) {
    return storeEmail(email)
  }
  $("#email").addClass("is-invalid")
  $("#error").addClass("text-danger").css('font-size', '12px').text("Email validation failed!")
  $("#email").effect("shake", { times: 3 }, 500)
})

