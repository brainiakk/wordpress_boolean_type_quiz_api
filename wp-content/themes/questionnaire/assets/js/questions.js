
var quizApp = function () {
    this.score = 0
    this.questionNumber = 1
    this.currentQuestion = 0
    this.totalQuestions = 0
    this.questions = {}
    this.userScores = {}
    this.userEmail = localStorage.getItem('userEmail')

    this.getScores = async function () {
        const url = 'wp-json/api/v1/scores'
        const config = { headers: { 'Content-Type': 'application/json' } }
        try {
            const request = await axios.get(url, config)
            if (request.data.success) {
                this.userScores = request?.data?.data
            } else {
                this.userScores = {}
            }
        } catch (error) {
            alert('Oops! Something went wrong, page would reload shortly...')
            location.reload()
        }
    }
    this.getQuestions = async function () {
        const url = "https://opentdb.com/api.php?amount=5&category=9&difficulty=easy&type=boolean"
        const config = { headers: { 'Content-Type': 'application/json' } }
        try {
            const request = await axios.get(url, config)
            this.questions = request?.data?.results
            this.totalQuestions = this.questions?.length
        } catch (error) {
            alert('Oops! Something went wrong, page would reload shortly...')
            location.reload()
        }
    }
    this.displayQuiz = function (currQuestion) {
        this.currentQuestion = currQuestion
        if (this.currentQuestion < this.totalQuestions) {
            $("#total-questions").html(this.totalQuestions)
            $("#question-number").html(this.questionNumber)
            const percentage = parseInt((100 / this.totalQuestions) * this.questionNumber)

            $("#quiz-progress").css('width', `${percentage}%`);
            $(".quiz-question").html(this.questions[this.currentQuestion].question)
        }
        if (this.currentQuestion >= this.totalQuestions) {
            $("#quiz-progress").css('width', '100%');
            return this.submitQuestionnaire()
        }
    }

    this.checkAnswer = function (option) {
        if (option == this.questions[this.currentQuestion].correct_answer) {
            this.score += 1
        }
    }

    this.changeQuestion = function (currQuestion) {
        this.currentQuestion = this.currentQuestion + currQuestion
        this.questionNumber = this.currentQuestion + 1
        this.displayQuiz(this.currentQuestion)
    }

    this.submitQuestionnaire = async function () {
        $('#timer').slideDown()
        $('input[name=answer]:radio').attr('disabled', true)
        const url = 'wp-json/api/v1/score/store'
        const config = { headers: { 'Content-Type': 'application/json' } }
        const payload = {
            "email": this.userEmail,
            "score": this.score
        }
        try {
            const request = await axios.post(url, payload, config)
            if (request.data.success) {
                this.showResult()
            } else {
                alert('Oops! Something went wrong, page would reload shortly...')
                location.reload()
            }
        } catch (error) {
            alert('Oops! Something went wrong, page would ssreload shortly...')
            location.reload()
        }
    }

    this.showResult = function () {
        $("#step-2").hide()
        $("#thankyou").show() // show next

        $("#user-score").html(`${this.score}/${this.totalQuestions}`)
        let dataSet = this.questions.map((question, index) => {
            return [index + 1, question.correct_answer, question.incorrect_answers[0]]
        })
        $('#questions-answers').DataTable({
            data: dataSet,
            paging: false,
            scrollY: 400,
            searching: false,
            destroy: true,
            columns: [
                { title: 'Question Number' },
                { title: 'Correct Answer' },
                { title: 'Incorrect Answers' },
            ],
        })


    }
}

let initQuiz = new quizApp()

$(document).ready(async function () {
    await initQuiz.getQuestions()
    await initQuiz.getScores()
    if (initQuiz.totalQuestions > 0) {
        $('#loader').addClass('d-none')
        initQuiz.displayQuiz(0)
        $('#quiz-box').removeClass('d-none').fadeIn()
        $('#timer').removeClass('d-none')
    }
    $('input[name=answer]:radio').on('click', function (e) {
        $(this).prop("checked", true)
        let selectedopt = $(this).val() in ["True", "False"] ? $(this).val() : 'False'
        if (selectedopt) {
            initQuiz.checkAnswer(selectedopt)
        }
        setTimeout(() => {
            initQuiz.changeQuestion(1)
            $(this).prop('checked', false);
            $('.form-input').removeClass('active-input');
        }, 500);

    })

    if (initQuiz.userScores.length > 0) {

        let dataSet = initQuiz.userScores.map((score, index) => {
            return [index + 1, score.email, score.score]
        })
        
        $('#score-board').DataTable({
            data: dataSet,
            scrollY: 400,
            destroy: true,
            columns: [
                { title: '#' },
                { title: 'Email Address' },
                { title: 'Score' },
            ],
        })
    }
})