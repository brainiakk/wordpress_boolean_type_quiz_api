<section class="steps-inner stepss pop-slide" id="step-2" data-page="1">

    <div class="col-md-12">
        <div class="wrapper">
            <div class="step-heading mb-4">
                <h2>Questionnaire</h2>
                <p>Fill out this Trivia quiz for fun!</p>
            </div>
            <div class="step-bar">
                <span class="step-counter">
                    Question <span id="question-number">1</span> / <span id="total-questions">4</span>
                </span>
                <div class="progress rounded-pill my-3">
                    <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" id="quiz-progress" aria-label="questionnaire progress-bar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <form action="#" method="post">
                <div class="form-heading quiz-question">

                </div>
                <div class="form-inner">
                    <label class="form-input" for="true">
                        <input type="radio" value="True" id="true" name="answer">
                        True
                    </label>
                    <label class="form-input" for="false">
                        <input type="radio" value="False" id="false" name="answer">
                        False
                    </label>
                </div>
            </form>
        </div>
    </div>
</section>