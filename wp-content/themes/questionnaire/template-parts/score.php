<section class=" stepss thankyou-circle steps-inner" id="thankyou" data-page="2">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper">
                <h2>Your Score: <span id="user-score"></span></h2>
                <div class="table-responsive">
                    <table id="questions-answers" class="table table-striped table-bordered" style="width:100%">

                    </table>
                </div>
                <div class="next-prev-btn">
                    <button class="next back" onclick="location.reload()">Try Again</button>
                </div>
            </div>
        </div>
    </div>
</section>