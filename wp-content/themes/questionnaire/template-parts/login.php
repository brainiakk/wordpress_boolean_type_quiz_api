<section class="steps-inner stepss pop-slide" id="step-1" data-page="0">
    <div class="wrapper">
        <div class="step-heading mb-4">
            <h2>Questionnaire</h2>
            <p>Provide an email address</p>
        </div>
        <form action="#" method="post" id="email_form">
            <div class="form-heading">
                <input type="email" class="form-control" placeholder="john.doe@email.com" name="email" id="email">
                <p><small id="error" class="my-3 is-small"></small></p>
                <button type="submit" class="btn btn-danger">Continue <i style="vertical-align: middle;" class="fa fa-arrow-right"></i></button>
            </div>
        </form>
    </div>
</section>