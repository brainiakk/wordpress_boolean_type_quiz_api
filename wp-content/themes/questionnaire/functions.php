<?php
// Auto import stylesheets and scripts for template

function qt_register_styles(){
    $version = wp_get_theme()->get('Version');

    wp_enqueue_style('qt-bootstrap', get_template_directory_uri()."/assets/css/bootstrap.min.css", array(), '5.1', 'all');
    wp_enqueue_style('qt-bootstrap', get_template_directory_uri()."/assets/vendors/bootstrap-datatable/datatables.min.css", array(), rand(999, 10129823), 'all');
    wp_enqueue_style('qt-style', get_template_directory_uri()."/assets/css/style.css", array(), $version, 'all');
    wp_enqueue_style('qt-responsive', get_template_directory_uri()."/assets/css/responsive.css", array(), '1.0', 'all');
    wp_enqueue_style('qt-animation', get_template_directory_uri()."/assets/css/animation.css", array(), '1.0', 'all');
    wp_enqueue_style('qt-fontawesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css", array(), '1.0', 'all');
    wp_enqueue_style('qt-fontawesome', "https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css", array(), '1.12.1', 'all');
}


add_action('wp_enqueue_scripts', 'qt_register_styles');


function qt_register_scripts(){
    wp_enqueue_script('qt-jquery', get_template_directory_uri()."/assets/js/jquery-3.6.0.min.js", array(), '3.6.0', true);
    wp_enqueue_script('qt-axios', "https://unpkg.com/axios/dist/axios.min.js", array(), '0.27', true);
    wp_enqueue_script('qt-jquery-ui', "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js", array(), '1.9.1', true);
    wp_enqueue_script('qt-jquery-data-tables', "https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js", array(), '1.9.1', true);
    wp_enqueue_script('qt-bootstrap', get_template_directory_uri()."/assets/js/bootstrap.min.js", array(), '5.1', true);
    wp_enqueue_script('qt-bootstrap-datatables', get_template_directory_uri()."/assets/vendors/bootstrap-datatable/datatables.min.js", array(), rand(999, 10129823), true);
    wp_enqueue_script('qt-custom', get_template_directory_uri()."/assets/js/custom.js", array(), rand(999, 10129823), true);
    wp_enqueue_script('qt-questions', get_template_directory_uri()."/assets/js/questions.js", array(), rand(999, 10129823), true);
}

add_action('wp_enqueue_scripts', 'qt_register_scripts');


// Automatically add new questionnaire table to database to save user's scores

function qt_create_questionnaire_table(){
    global $wpdb;

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $tableName = $wpdb->prefix . "quiz_scores";  // Get the database table prefix to create my new table
	$charsetCollate = $wpdb->get_charset_collate(); // Get the default db character set collation

    $sql = "CREATE TABLE IF NOT EXISTS $tableName (
      id int(10) unsigned NOT NULL AUTO_INCREMENT,
      email varchar(255) NOT NULL,
      score int(11) NOT NULL,
      created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY  (id)
    ) ".$charsetCollate;

    dbDelta($sql);
}

add_action("after_switch_theme", "qt_create_questionnaire_table");


// Register custom API routes to handle user quiz_scores

// POST Request starts
add_action('init', function () {
    register_rest_route('/api/v1', '/score/store', array(
        'methods' => 'POST',
        'callback' => 'qt_api_store_callback',
    ));
});

function qt_api_store_callback(WP_REST_Request $request) {
    global $wpdb;
    $table = $wpdb->prefix . 'quiz_scores';
    $parameters = $request->get_params();
    $email = $parameters['email'];
    $score = $parameters['score'];

    $wpdb->insert($table, array(
        'email' => $email,
        'score' => $score
    ));
    $response = [];
    $response['status'] = 200;
    $response['success'] = true;
    $response['data'] = $request->get_params();
    return new WP_REST_Response($response);
}
// POST Request ends


// GET Request Starts
add_action('init', function () {
    register_rest_route('/api/v1', '/scores', array(
        'methods' => 'GET',
        'callback' => 'qt_api_get_callback',
    ));
});

function qt_api_get_callback($request) {
    global $wpdb;
    $table = $wpdb->prefix . 'quiz_scores';
    $query = "SELECT * FROM $table";
    $results = $wpdb->get_results($query);

    $response = [];
    $response = count($results) > 0 ? 
    ['status' => 200, 'success' => true, 'data' => $results] : 
    ['status' => 200, 'success' => false, 'message' => 'No results found'];
    return new WP_REST_Response($response);
}

// GET Request ends
